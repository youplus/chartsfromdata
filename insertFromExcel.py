import pandas as pd
import json
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import re
from charts import pieChart
from charts import columnChart
from charts import verbatims
from charts import insight
from charts import html
from charts import columnChartMultiple
import copy
import math

filenames = ["Automotive_Final","B2B_final","Brand_Final","Health_Final","In-Market_Final","Interests_Final"]

#filenames = ["Brand"]
order_number=1
for filename in filenames:
   xls = pd.ExcelFile(filename+".xlsx")



   study_id="5dd7bb4759af316c92147c1d"
   country ="US"
   product_type="study_country_question"



   for sheet in xls.sheet_names :
      
      df = pd.read_excel (filename+".xlsx",header=None,sheetname=sheet, skiprows=[0,1,2])
      categories = []
      print(sheet)

      rowSize = len(df.index)

      columnSize = int(int(str(df.size))/rowSize)


      seriesValue=[]


      if(rowSize <=3):

         
         mapBarsList = []
         mapBars={}
         data=[]
         for i in range (1, columnSize-1):
            for j in range (0 , rowSize-1):
               if j==0:
                  mapBarsInstance =copy.deepcopy(mapBars)
                  dataInstance =copy.deepcopy(data)
                  mapBarsInstance["name"]= df[i][0]
               
               else:
                  if math.isnan(df[i][j]):
                     df[i][j]=0
                  mapBarsInstance["y"]=round(df[i][j],1)
                  mapBarsInstance["color"]="#50ADE8"

            mapBarsList.append(mapBarsInstance)
         colChart = columnChart.ColumnChart()
         jsonInst= {}
         jsonInst["Title"] = filename +" : "+sheet
         jsonInst["values"]=mapBarsList
         colChart.insertBasicColumn(study_id,country,1, order_number,jsonInst)



      else:

         for category in df[0]:
            if (category != "Row Labels" and category!="Grand Total"):
               categories.append(category)

         mapBarsList = []
         mapBars={}
         data=[]
         for i in range (1, columnSize-1):
            for j in range (0 , rowSize-1):
               if j==0:
                  mapBarsInstance =copy.deepcopy(mapBars)
                  dataInstance =copy.deepcopy(data)
                  mapBarsInstance["name"]= df[i][0]
                 

               else:
                  if math.isnan(df[i][j]):
                     df[i][j]=0
                  dataInstance.append(round(df[i][j],1))
                  mapBarsInstance["data"]=dataInstance

            mapBarsList.append(mapBarsInstance)
         
         colChartMultiple = columnChartMultiple.ColumnChartMultiple()
         title=filename +" : "+sheet
         colChartMultiple.insertBasicColumnMultiple(study_id,country,categories,1,"Propensity Index", order_number,mapBarsList,title)
      order_number=order_number+1

print("final order number is ", order_number)



                        




                
  
   
      
  
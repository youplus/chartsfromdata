from pymongo import MongoClient
from bson.objectid import ObjectId

from util.db import DB


class ColumnChartMultiple:
    def insertBasicColumnMultiple(self,study_id,country,categories,question_order_number,yaxisName, order_number,seriesValue,title):
        obj = ObjectId()

        data = {
        "_id": obj,
        "study_id" : ObjectId(study_id),
        "country" : country,
        "product_type" : "study_country_question",
        "approved" : False,
        "order_number" : order_number,
        "width" : 12,# 12 for non charts, 6 for charts type
        "config" : {
            "series" : seriesValue,
                "tooltip": {
        "headerFormat": '<span style="font-size:10px">{point.key}</span><table>',
        "pointFormat": '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        "footerFormat": '</table>',
        "shared": True,
        "useHTML": True
    },
                
            
            "xAxis" : {
                "labels" : {
                    "style" : {
                        "color" : "#fff",
                        "fontFamily" : "Helvetica Neue, sans-serif",
                        "fontSize" : 12.0
                    }
                },
                "categories":categories
            },
            "plotOptions" : {
                "column" : {
                    "dataLabels" : {
                        "format" : "{y}",
                        "style" : {
                            "fontSize" : "14"
                        },
                        "color" : "#fff",
                        "enabled" : True
                    },
                    "showInLegend" : True,
                    "allowPointSelect" : True,
                    "borderColor" : "transparent",
                    "cursor" : "pointer"
                },
            },
            "credits" : False,
            "legend" : {
                "enabled" : True,
                "itemStyle":{"color": "#FFF"}
            },
            "subtitle" : {
                "align" : "left",
                "style" : {
                    "color" : "#7A7C87",
                    "fontFamily" : "Montserrat",
                    "fontSize" : "1.1em",
                    "fontWeight" : "400",
                    "letterSpacing" : "0.05em"
                },
                "text" : ""
            },
            "title" : {
                "align" : "left",
                "style" : {
                    "color" : "#D3D3D7",
                    "fontFamily" : "Montserrat",
                    "fontSize" : "1.6em",
                    "fontWeight" : 400.0
                },
                "text" : title
            },
            "yAxis" : [
                {
                    "gridLineWidth" : 0.0,
                    "index" : 0.0,
                    "lineColor" : "#666666",
                    "lineWidth" : 1.0,
                    "title" : {
                        "text" : yaxisName
                    }
                }
            ],
            "chart" : {
                "plotShadow" : False,
                "style" : {
                    "fontFamily" : "Helvetica Neue, sans-serif",
                    "fontWeight" : "bold"
                },
                "type" : "column",
                "backgroundColor" : "#222437",
                "height" : "405px",
                "plotBackgroundColor" : "#222437",
                "plotBorderWidth" : None
            }
        },
        "default_config" : {
            "chart" : {
                "style" : {
                    "fontFamily" : "Helvetica Neue, sans-serif",
                    "fontWeight" : "bold"
                },
                "backgroundColor" : "#222437",
                "plotBackgroundColor" : "#222437",
                "height" : "405px",
                "plotBorderWidth" : None,
                "plotShadow" : False,
                "type" : "column"
            },
            "title" : {
                "align" : "left",
                "style" : {
                    "fontFamily" : "Montserrat",
                    "fontSize" : "1.6em",
                    "fontWeight" : 400.0,
                    "color" : "#D3D3D7"
                },
                "text" : "title test"
            },
            "plotOptions" : {
                "column" : {
                    "allowPointSelect" : True,
                    "cursor" : "pointer",
                    "showInLegend" : True,
                    "borderColor" : "transparent",
                    "dataLabels" : {
                        "style" : {
                            "fontSize" : "14"
                        },
                        "enabled" : True,
                        "color" : "#fff",
                        "format" : "{y}"
                    }
                }
            },
            "credits" : False,
            "subtitle" : {
                "align" : "left",
                "style" : {
                    "fontFamily" : "Montserrat",
                    "fontSize" : "1.1em",
                    "letterSpacing" : "0.05em",
                    "color" : "#7A7C87",
                    "fontWeight" : "400"
                },
                "text" : ""
            },
            "xAxis" : {
                "type" : "category",
                "lineColor" : "#666666",
                "labels" : {
                    "style" : {
                        "fontSize" : 12.0,
                        "fontFamily" : "Helvetica Neue, sans-serif",
                        "color" : "#fff"
                    }
                }
            },
            "yAxis" : [
                {
                    "gridLineWidth" : 0.0,
                    "lineColor" : "#666666",
                    "lineWidth" : 1.0,
                    "title" : {
                        "text" : yaxisName
                    },

                }
            ],
            "legend" : {
                "enabled" : True,
                "itemStyle":{"color": "#FFF"}

            },
            "series" : [
                {
                    "name" : yaxisName,
                    "colorByPoint" : True,
                    "type" : "column",
                    "series" : seriesValue,
                    "marker" : {
                        "fillColor" : ""
                    }
                }
            ]
        },
        "question_order_number" : question_order_number,
        "insights" : [], # insights
        "section" : "",# section
        "verbatims" : [],# verbatims
        "html" : "",# html
        "type" : "charts",# charts/html/section/verbatims/insights/
        "chart_type" : "COLUMN", #pie/column_with_spline/column/spline/bar  - when type is charts
        
    }

        dbInstance = DB()

        dbInstance.insertChart(data)
from pymongo import MongoClient
from bson.objectid import ObjectId

from util.db import DB


class ColumnChart:


  

    def insertBasicColumn(self,study_id,country,question_order_number, order_number,json):
        obj = ObjectId()

        data = {
        "_id": obj,
        "study_id" : ObjectId(study_id),
        "country" : country,
        "product_type" : "study_country_question",
        "approved" : False,
        "order_number" : order_number,
        "width" : 12,# 12 for non charts, 6 for charts type
        "config" : {
            "series" : [
                {
                    "type" : "column",
                    "colorByPoint" : True,
                    "data" : json["values"],
                    "marker" : {
                        "fillColor" : ""
                    },
                    "name" : "Propensity Index"
                },
            ],
            "xAxis" : {
                "labels" : {
                    "style" : {
                        "color" : "#fff",
                        "fontFamily" : "Helvetica Neue, sans-serif",
                        "fontSize" : 12.0
                    }
                },
                "lineColor" : "#666666",
                "type" : "category"
            },
            "plotOptions" : {
                "column" : {
                    "dataLabels" : {
                        "format" : "{y}",
                        "style" : {
                            "fontSize" : "14"
                        },
                        "color" : "#fff",
                        "enabled" : True
                    },
                    "showInLegend" : True,
                    "allowPointSelect" : True,
                    "borderColor" : "transparent",
                    "cursor" : "pointer"
                }
            },
            "credits" : False,
            "legend" : {
                "enabled" : False
            },
            "subtitle" : {
                "align" : "left",
                "style" : {
                    "color" : "#7A7C87",
                    "fontFamily" : "Montserrat",
                    "fontSize" : "1.1em",
                    "fontWeight" : "400",
                    "letterSpacing" : "0.05em"
                },
                "text" : ""
            },
            "title" : {
                "align" : "left",
                "style" : {
                    "color" : "#D3D3D7",
                    "fontFamily" : "Montserrat",
                    "fontSize" : "1.6em",
                    "fontWeight" : 400.0
                },
                "text" : json["Title"]
            },
            "yAxis" : [
                {
                    "gridLineWidth" : 0.0,
                    "index" : 0.0,
                    "lineColor" : "#666666",
                    "lineWidth" : 1.0,

                    "title" : {
                        "text" : "Propensity Index"
                    }
                }
            ],
            "chart" : {
                "plotShadow" : False,
                "style" : {
                    "fontFamily" : "Helvetica Neue, sans-serif",
                    "fontWeight" : "bold"
                },
                "type" : "column",
                "backgroundColor" : "#222437",
                "height" : "405px",
                "plotBackgroundColor" : "#222437",
                "plotBorderWidth" : None
            }
        },
        "default_config" : {
            "chart" : {
                "style" : {
                    "fontFamily" : "Helvetica Neue, sans-serif",
                    "fontWeight" : "bold"
                },
                "backgroundColor" : "#222437",
                "plotBackgroundColor" : "#222437",
                "height" : "405px",
                "plotBorderWidth" : None,
                "plotShadow" : False,
                "type" : "column"
            },
            "title" : {
                "align" : "left",
                "style" : {
                    "fontFamily" : "Montserrat",
                    "fontSize" : "1.6em",
                    "fontWeight" : 400.0,
                    "color" : "#D3D3D7"
                },
                "text" : json["Title"]
            },
            "plotOptions" : {
                "column" : {
                    "allowPointSelect" : True,
                    "cursor" : "pointer",
                    "showInLegend" : True,
                    "borderColor" : "transparent",
                    "dataLabels" : {
                        "style" : {
                            "fontSize" : "14"
                        },
                        "enabled" : True,
                        "color" : "#fff",
                        "format" : "{y}"
                    }
                },
            },
            "credits" : False,
            "subtitle" : {
                "align" : "left",
                "style" : {
                    "fontFamily" : "Montserrat",
                    "fontSize" : "1.1em",
                    "letterSpacing" : "0.05em",
                    "color" : "#7A7C87",
                    "fontWeight" : "400"
                },
                "text" : ""
            },
            "xAxis" : {
                "type" : "category",
                "lineColor" : "#666666",
                "labels" : {
                    "style" : {
                        "fontSize" : 12.0,
                        "fontFamily" : "Helvetica Neue, sans-serif",
                        "color" : "#fff"
                    }
                }
            },
            "yAxis" : [
                {
                    "gridLineWidth" : 0.0,
                    "lineColor" : "#666666",
                    "lineWidth" : 1.0,
                    "title" : {
                        "text" : "Propensity Index"
                    },

                }
            ],
            "legend" : {
                "enabled" : False
            },
            "series" : [
                {
                    "name" : "Propensity Index",
                    "colorByPoint" : True,
                    "type" : "column",
                    "data" : json["values"],
                    "marker" : {
                        "fillColor" : ""
                    }
                }
            ]
        },
        "question_order_number" : question_order_number,
        "insights" : [], # insights
        "section" : "",# section
        "verbatims" : [],# verbatims
        "html" : "",# html
        "type" : "charts",# charts/html/section/verbatims/insights/
        "chart_type" : "COLUMN", #pie/column_with_spline/column/spline/bar  - when type is charts
        "test2" : "test2",
        
    }

        dbInstance = DB()
        dbInstance.insertChart(data)
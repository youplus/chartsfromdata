from bson.objectid import ObjectId
from pymongo import MongoClient
from bson.objectid import ObjectId
from util.db import DB


class PieCh:

    def pieChartType(self):
        nameList = []
        data = {
        "Title": "GG wash Vs Machine wash",
        "subTitle": "",
        "Type": "Pie",
        "titleColor": "#50ade8",
        "Width": {
        "lg": 6,
        "xl": 6,
        "xs": 24
        },
        "Height": "444px",
        "values": [
        {
        "name": "Machine wash",
        "y":66,
        "color": "#50ade8",
        "sliced": ""
        },
        {
        "name": "Hand wash",
        "y": 34,
        "color": "#fbaf35",
        "sliced": ""
        }
        ]
        }

        title = data["Title"]
        for names in data["values"]:
            nameList.append(names)
    #     source_coll.insert_one(data)
        return nameList,title



    def insertPieChart(self,study_id,country,question_order_number, order_number,json):
        
        obj = ObjectId()
        print(obj)
        data = {
            "_id": obj,
            "study_id" : ObjectId(study_id),
            "country" : country,
            "product_type" : "study_country_question",
    #         "attributes" : [],
    #         "group_attributes" : [
    #             "Others",
    #             "Hardware/Design",
    #             "Display/Resolution",
    #             "Camera"
    #         ],
            "approved" : True,
            "order_number" : order_number,
            "width" : 12,# 12 for non charts, 6 for charts type
            "config" : {
        "chart":{
            "plotBorderWidth":None,
            "plotShadow":False,
            "type":"pie",
            "style":{
                "fontFamily":"Helvetica Neue, sans-serif",
                "fontWeight":"bold"
            },
            "backgroundColor":"#222437",
            "plotBackgroundColor":"#222437",
            "height":"405px"
        },
        "title":{
            "align":"left",
            "style":{
                "fontFamily":"Montserrat",
                "fontSize":"1.6em",
                "fontWeight":400,
                "color":"#D3D3D7"
            },
            "text":json["Title"]
        },
        "subtitle":{
            "align":"left",
            "style":{
                "fontFamily":"Montserrat",
                "fontSize":"1.1em",
                "letterSpacing":"0.05em",
                "color":"#7A7C87",
                "fontWeight":"400"
            },
            "text":""
        },
        "exporting":{
            "chartOptions":{
                "plotOptions":{
                    "series":{
                    "dataLabels":{
                        "enabled":True
                    }
                    }
                }
            },
            "fallbackToExportServer":False
        },
        "tooltip":{
            "pointFormat":"{series.name}: <b>{point.percentage:.1f}</b>"
        },
        "plotOptions":{
            "pie":{
                "allowPointSelect":True,
                "cursor":"pointer",
                "colors":[
                    "#EFA226",
                    "#59AEBA",
                    "#62C7A4",
                    "#BFB040",
                    "#288FBC",
                    "#EFA226"
                ],
                "showInLegend":True,
                "borderColor":"transparent",
                "dataLabels":{
                    "enabled":True,
                    "format":"<br>{point.percentage:.1f} %",
                    "distance":-50,
                    "filter":{
                    "property":"percentage",
                    "operator":">",
                    "value":4
                    },
                    "style":{
                    "fontFamily":"sans-serif",
                    "color":"#fff"
                    }
                },
                "events":{

                }
            }
        },
        "legend":{
            "layout":"horizontal",
            "itemStyle":{
                "color":"#fff"
            },
            "itemMarginTop":7,
            "itemHoverStyle":{
                "color":"#000"
            }
        },
        "series":[
            {
                "name":"Volumetric",
                "colorByPoint":True,
                "data":json["values"],
                "color":"#000",
                "dataLabels":{
                    "enabled":True,
                    "format":"{y}%",
                    "style":{
                    "textOutline":0,
                    "color":"#000",
                    "fontSize":14
                    }
                }
            }
        ],
        "credits":{
            "enabled":False
        }
        },
            "default_config" : {
                "chart" : {
                    "style" : {
                        "fontFamily" : "Helvetica Neue, sans-serif",
                        "fontWeight" : "bold"
                    },
                    "backgroundColor" : "#222437",
                    "plotBackgroundColor" : "#222437",
                    "height" : "405px",
                    "plotBorderWidth" : None,
                    "plotShadow" : False,
                    "type" : "pie"
                },
                "title" : {
                    "align" : "left",
                    "style" : {
                        "fontFamily" : "Montserrat",
                        "fontSize" : "1.6em",
                        "fontWeight" : 400.0,
                        "color" : "#D3D3D7"
                    },
                    "text" : json["Title"]
                },
                "plotOptions" : {
                    "column" : {
                        "allowPointSelect" : True,
                        "cursor" : "pointer",
                        "showInLegend" : True,
                        "borderColor" : "transparent",
                        "dataLabels" : {
                            "style" : {
                                "fontSize" : "14"
                            },
                            "enabled" : True,
                            "color" : "#fff",
                            "format" : "{y}%"
                        }
                    },
                    "spline" : {
                        "events" : {},
                        "dataLabels" : {
                            "style" : {
                                "fontSize" : "14"
                            },
                            "enabled" : True,
                            "color" : "#fff",
                            "format" : "{y}"
                        }
                    }
                },
                "credits" : False,
                "subtitle" : {
                    "align" : "left",
                    "style" : {
                        "fontFamily" : "Montserrat",
                        "fontSize" : "1.1em",
                        "letterSpacing" : "0.05em",
                        "color" : "#7A7C87",
                        "fontWeight" : "400"
                    },
                    "text" : ""
                },
                "xAxis" : {
                    "type" : "category",
                    "lineColor" : "#666666",
                    "labels" : {
                        "style" : {
                            "fontSize" : 12.0,
                            "fontFamily" : "Helvetica Neue, sans-serif",
                            "color" : "#fff"
                        }
                    }
                },
                "yAxis" : [
                    {
                        "gridLineWidth" : 0.0,
                        "lineColor" : "#666666",
                        "lineWidth" : 1.0,
                        "title" : {
                            "text" : "Volumetric"
                        },
                        "max" : 100.0
                    },
                    {
                        "lineWidth" : 1.0,
                        "title" : {
                            "text" : "TFS"
                        },
                        "opposite" : True,
                        "gridLineWidth" : 0.0,
                        "lineColor" : "#666666"
                    }
                ],
                "legend" : {
                    "enabled" : False
                },
                "series" : [
                    {
                        "name" : "Volumetric",
                        "colorByPoint" : True,
                        "type" : "pie",
                        "data" : json["values"],
                        "marker" : {
                            "fillColor" : ""
                        }
                    },
                    {
                        "name" : "TFS",
                        "type" : "pie",
                        "data" : json["values"],
                        "lineColor" : "#7FDCFE",
                        "marker" : {
                            "fillColor" : "#7FDCFE"
                        },
                        "yAxis" : 1.0
                    }
                ]
            },
            "question_order_number" : question_order_number,
            "insights" : [], # insights
            "section" : "",# section
            "verbatims" : [],# verbatims
            "html" : "",# html
            "type" : "charts",# charts/html/section/verbatims/insights/
            "chart_type" : "pie", #pie/column_with_spline/column/spline/bar  - when type is charts

        }
        dbInstance = DB()
        print(data)
        dbInstance.insertChart(data)

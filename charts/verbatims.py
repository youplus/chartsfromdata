from bson import ObjectId
from pymongo import MongoClient
from bson.objectid import ObjectId
from util.db import DB


class VerbatimsType:




    def insertVerbatimsChart(self,study_id,country,question_order_number,order_number,json):
        obj = ObjectId()
        print(obj)
        data = {
            "_id": obj,
            "study_id" : ObjectId(study_id),
            "country" : country,
            "product_type" : "study_country_question",
    #         "attributes" : [],
    #         "group_attributes" : [
    #             "Others",
    #             "Hardware/Design",
    #             "Display/Resolution",
    #             "Camera"
    #         ],
            "approved" : True,
            "order_number" : order_number,
            "width" : 12,# 12 for non charts, 6 for charts type
            "config" : None,
            "default_config" : None,
            "question_order_number" : question_order_number,
            "insights" : [], # insights
            "section" : "",# section
            "verbatims" : json["values"],# verbatims
            "html" : "", # html
            "type" : "verbatims", # charts/html/section/verbatims/insights/
            "chart_type" : "", #pie/column_with_spline/column/spline/bar  - when type is charts
            "chart_contents" : "",
            "type_scores" : [],
            "type_volumes" : []

        }

        
        dbInstance = DB()
        dbInstance.insertChart(data)
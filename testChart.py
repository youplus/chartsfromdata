import pandas as pd
import json
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import re
from charts import pieChart
from charts import columnChart
from charts import verbatims
from charts import insight
from charts import html
from charts import columnChartMultiple


study_id="5d9ed568f11e71cc0314e958"
country ="US"
product_type="study_country_question"

question_order_number= 1
yaxisName = "Propensity Index"
seriesValue = [{
        "name": 'Tokyo',
        "data": [49.9, 71.5, 106.4, 129.2]

    }, {
        "name": 'New York',
        "data": [83.6, 78.8, 98.5, 93.4]

    }, {
        "name": 'London',
        "data": [48.9, 38.8, 39.3, 41.4]

    }, {
        "name": 'Berlin',
        "data": [42.4, 33.2, 34.5, 39.7]

    }]
categories =  [
            'Jan',
            'Feb',
            'Mar',
            'Apr'
        ]
seriesValue = [{'data': [1.2, 1.2, 3.2], 'name': ' Consumer Profile '}, {'data': [0.8, 0.8, 5.9], 'name': ' In-Market '}, {'data': [1.4, 1.4, 4.3], 'name': ' Makes '}, {'data': [0.5, 0.6, 2.4], 'name': ' Parts & Services '}, {'data': [1.5, 1.5, 3.7], 'name': ' Types '}]
categories =  ['13-17 (Teens)', '18-24 (Transitionals)', '25 - 34 (Adults)']
colChartMultiple = columnChartMultiple.ColumnChartMultiple()
colChartMultiple.insertBasicColumnMultiple(study_id,country,categories,question_order_number,yaxisName, question_order_number,seriesValue)

jsonInst= {}
jsonInst["values"]=[{'color': '#50ADE8', 'name': ' Acura', 'y': 1.5}, {'color': '#50ADE8', 'name': ' Acura ', 'y': 7.4}, {'color': '#50ADE8', 'name': ' Alfa Romeo', 'y': 2.6}, {'color': '#50ADE8', 'name': ' Alfa Romeo ', 'y': 6.2}, {'color': '#50ADE8', 'name': ' Aston Martin', 'y': 2.5}, {'color': '#50ADE8', 'name': ' Aston Martin ', 'y': 12.8}, {'color': '#50ADE8', 'name': ' Audi', 'y': 1.8}, {'color': '#50ADE8', 'name': ' Audi ', 'y': 21.1}, {'color': '#50ADE8', 'name': ' Bentley', 'y': 3.5}, {'color': '#50ADE8', 'name': ' Bentley ', 'y': 6.9}, {'color': '#50ADE8', 'name': ' BMW', 'y': 0.3}, {'color': '#50ADE8', 'name': ' BMW ', 'y': 6.2}, {'color': '#50ADE8', 'name': ' Buick', 'y': 0.6}, {'color': '#50ADE8', 'name': ' Buick ', 'y': 5.5}, {'color': '#50ADE8', 'name': ' Cadillac', 'y': 1.9}, {'color': '#50ADE8', 'name': ' Cadillac ', 'y': 6.6}, {'color': '#50ADE8', 'name': ' Chevrolet', 'y': 1.4}, {'color': '#50ADE8', 'name': ' Chevrolet ', 'y': 26.0}, {'color': '#50ADE8', 'name': ' Chrysler', 'y': 1.6}, {'color': '#50ADE8', 'name': ' Chrysler ', 'y': 3.3}, {'color': '#50ADE8', 'name': ' Dodge', 'y': 1.8}, {'color': '#50ADE8', 'name': ' Dodge ', 'y': 8.3}, {'color': '#50ADE8', 'name': ' Ferrari', 'y': 2.4}, {'color': '#50ADE8', 'name': ' Ferrari ', 'y': 20.6}, {'color': '#50ADE8', 'name': ' Fiat', 'y': 3.2}, {'color': '#50ADE8', 'name': ' Fiat ', 'y': 8.2}, {'color': '#50ADE8', 'name': ' Ford', 'y': 1.9}, {'color': '#50ADE8', 'name': ' Ford ', 'y': 23.3}, {'color': '#50ADE8', 'name': ' GMC', 'y': 0.3}, {'color': '#50ADE8', 'name': ' GMC ', 'y': 2.4}, {'color': '#50ADE8', 'name': ' Honda', 'y': 1.3}, {'color': '#50ADE8', 'name': ' Honda ', 'y': 22.2}, {'color': '#50ADE8', 'name': ' Hyundai', 'y': 2.6}, {'color': '#50ADE8', 'name': ' Hyundai ', 'y': 15.5}, {'color': '#50ADE8', 'name': ' Infiniti', 'y': 1.0}, {'color': '#50ADE8', 'name': ' Infiniti ', 'y': 12.6}, {'color': '#50ADE8', 'name': ' Jaguar', 'y': 2.1}, {'color': '#50ADE8', 'name': ' Jaguar ', 'y': 11.4}, {'color': '#50ADE8', 'name': ' Jeep', 'y': 1.2}, {'color': '#50ADE8', 'name': ' Jeep ', 'y': 6.3}, {'color': '#50ADE8', 'name': ' Kia', 'y': 1.4}, {'color': '#50ADE8', 'name': ' Kia ', 'y': 12.1}, {'color': '#50ADE8', 'name': ' Lamborghini', 'y': 2.7}, {'color': '#50ADE8', 'name': ' Land Rover', 'y': 2.0}, {'color': '#50ADE8', 'name': ' Land Rover ', 'y': 6.9}, {'color': '#50ADE8', 'name': ' Lexus', 'y': 1.5}, {'color': '#50ADE8', 'name': ' Lexus ', 'y': 14.7}, {'color': '#50ADE8', 'name': ' Lincoln', 'y': 1.7}, {'color': '#50ADE8', 'name': ' Lincoln ', 'y': 10.6}, {'color': '#50ADE8', 'name': ' Lotus', 'y': 2.2}, {'color': '#50ADE8', 'name': ' Maserati', 'y': 1.8}, {'color': '#50ADE8', 'name': ' Mazda', 'y': 2.7}, {'color': '#50ADE8', 'name': ' Mazda ', 'y': 5.7}, {'color': '#50ADE8', 'name': ' Mercedes Benz', 'y': 2.7}, {'color': '#50ADE8', 'name': ' Mercedes Benz ', 'y': 11.3}, {'color': '#50ADE8', 'name': ' Mini', 'y': 1.2}, {'color': '#50ADE8', 'name': ' Mini ', 'y': 7.0}, {'color': '#50ADE8', 'name': ' Mitsubishi', 'y': 1.3}, {'color': '#50ADE8', 'name': ' Mitsubishi ', 'y': 8.8}, {'color': '#50ADE8', 'name': ' Nissan', 'y': 1.3}, {'color': '#50ADE8', 'name': ' Nissan ', 'y': 12.8}, {'color': '#50ADE8', 'name': ' Porsche', 'y': 1.8}, {'color': '#50ADE8', 'name': ' Porsche ', 'y': 8.2}, {'color': '#50ADE8', 'name': ' Ram', 'y': 0.3}, {'color': '#50ADE8', 'name': ' Ram ', 'y': 2.7}, {'color': '#50ADE8', 'name': ' Rolls Royce', 'y': 3.9}, {'color': '#50ADE8', 'name': ' Scion', 'y': 1.5}, {'color': '#50ADE8', 'name': ' Scion ', 'y': 7.8}, {'color': '#50ADE8', 'name': ' Subaru', 'y': 1.5}, {'color': '#50ADE8', 'name': ' Subaru ', 'y': 10.9}, {'color': '#50ADE8', 'name': ' Tesla', 'y': 2.1}, {'color': '#50ADE8', 'name': ' Tesla ', 'y': 5.9}, {'color': '#50ADE8', 'name': ' Toyota', 'y': 1.6}, {'color': '#50ADE8', 'name': ' Toyota ', 'y': 17.3}, {'color': '#50ADE8', 'name': ' Volkswagen', 'y': 2.0}, {'color': '#50ADE8', 'name': ' Volkswagen ', 'y': 14.3}, {'color': '#50ADE8', 'name': ' Volvo', 'y': 1.4}, {'color': '#50ADE8', 'name': ' Volvo ', 'y': 11.9}]

colChart = columnChart.ColumnChart()
jsonInst["Title"] = "Information looked out"
colChart.insertBasicColumn(study_id,country,question_order_number, 1,jsonInst)






